use crate::ag::sqrt::Sqrt;
use rand::{distributions::*, prelude::*};
use std::{
    fmt::*,
    ops::{
        Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Range, Sub, SubAssign,
    },
};

#[derive(Debug, Default, Copy, Clone, PartialEq)]
pub struct Vec3<T> {
    pub x: T,
    pub y: T,
    pub z: T,
}

impl<T: Copy + Mul<Output = T> + Add<Output = T> + Sub<Output = T>> Vec3<T> {
    pub fn new(x: T, y: T, z: T) -> Self {
        Self { x, y, z }
    }

    #[inline(always)]
    pub fn dot(first: Vec3<T>, other: Vec3<T>) -> T {
        first.x * other.x + first.y * other.y + first.z * other.z
    }

    pub fn cross(first: Vec3<T>, other: Vec3<T>) -> Vec3<T> {
        Vec3 {
            x: first.y * other.z - first.z * other.y,
            y: first.z * other.x - first.x * other.z,
            z: first.x * other.y - first.y * other.x,
        }
    }
}

impl<T: uniform::SampleUniform + PartialOrd + Clone> Vec3<T>
where
    Standard: Distribution<T>,
{
    pub fn rand(range: Range<T>) -> Vec3<T> {
        let mut rand = rand::thread_rng();
        Vec3 {
            x: rand.gen_range(range.clone()),
            y: rand.gen_range(range.clone()),
            z: rand.gen_range(range),
        }
    }
}

impl Vec3<f64> {
    pub fn near_zero(&self) -> bool {
        let s = 1e-8;
        (self.x.abs() < s) && (self.y.abs() < s) && (self.z.abs() < s)
    }
}

impl<T: Sqrt + Mul<Output = T> + Add<Output = T> + Div<Output = T> + Copy> Vec3<T> {
    #[inline(always)]
    pub fn len_squared(&self) -> T {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    #[inline(always)]
    pub fn len(&self) -> T {
        self.len_squared().sqrt()
    }

    #[inline(always)]
    pub fn unit(&self) -> Vec3<T> {
        *self / self.len()
    }
}

impl<T: Display> Display for Vec3<T> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{} {} {}", self.x, self.y, self.z)
    }
}

impl<T: Clone> From<T> for Vec3<T> {
    fn from(from: T) -> Self {
        Vec3 {
            x: from.clone(),
            y: from.clone(),
            z: from,
        }
    }
}

impl<T: Copy> From<[T; 3]> for Vec3<T> {
    fn from(from: [T; 3]) -> Self {
        Vec3 {
            x: from[0],
            y: from[1],
            z: from[2],
        }
    }
}

impl<T> From<(T, T, T)> for Vec3<T> {
    fn from(from: (T, T, T)) -> Self {
        Vec3 {
            x: from.0,
            y: from.1,
            z: from.2,
        }
    }
}

impl<T: Copy> IntoIterator for Vec3<T> {
    type Item = T;
    type IntoIter = Vec3IntoIterator<T>;

    fn into_iter(self) -> Self::IntoIter {
        Vec3IntoIterator {
            data: self,
            index: 0,
        }
    }
}

pub struct Vec3IntoIterator<T> {
    data: Vec3<T>,
    index: usize,
}

impl<T: Copy> Iterator for Vec3IntoIterator<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        let result = match self.index {
            0 => &self.data.x,
            1 => &self.data.y,
            2 => &self.data.z,
            _ => return None,
        };

        self.index += 1;
        Some(*result)
    }
}

impl<T> Index<usize> for Vec3<T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            _ => panic!("Index out of bounds"),
        }
    }
}

impl<T> IndexMut<usize> for Vec3<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.x,
            1 => &mut self.y,
            2 => &mut self.z,
            _ => panic!("Index out of bounds"),
        }
    }
}

impl<T: Add<Output = T>> Add<Vec3<T>> for Vec3<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, other: Vec3<T>) -> Self::Output {
        Vec3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl<T: Neg + Neg<Output = T>> Neg for Vec3<T> {
    type Output = Self;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        Vec3 {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl<T: AddAssign + Copy> AddAssign<Vec3<T>> for Vec3<T> {
    fn add_assign(&mut self, other: Vec3<T>) {
        self.x += other.x;
        self.y += other.y;
        self.z += other.z;
    }
}

impl<T: Sub<Output = T>> Sub<Vec3<T>> for Vec3<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, other: Vec3<T>) -> Self::Output {
        Vec3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl<T: SubAssign + Copy> SubAssign<Vec3<T>> for Vec3<T> {
    #[inline(always)]
    fn sub_assign(&mut self, other: Vec3<T>) {
        self.x -= other.x;
        self.y -= other.y;
        self.z -= other.z;
    }
}

impl<T: Mul<Output = T>> Mul<Vec3<T>> for Vec3<T> {
    type Output = Self;

    #[inline(always)]
    fn mul(self, other: Vec3<T>) -> Self::Output {
        Vec3 {
            x: self.x * other.x,
            y: self.y * other.y,
            z: self.z * other.z,
        }
    }
}

impl<T: MulAssign + Copy> MulAssign<Vec3<T>> for Vec3<T> {
    #[inline(always)]
    fn mul_assign(&mut self, other: Vec3<T>) {
        self.x *= other.x;
        self.y *= other.y;
        self.z *= other.z;
    }
}

impl<T: Mul<Output = T> + Copy> Mul<T> for Vec3<T> {
    type Output = Self;

    #[inline(always)]
    fn mul(self, other: T) -> Self::Output {
        Vec3 {
            x: self.x * other,
            y: self.y * other,
            z: self.z * other,
        }
    }
}

impl<T: MulAssign + Copy> MulAssign<T> for Vec3<T> {
    #[inline(always)]
    fn mul_assign(&mut self, other: T) {
        self.x *= other;
        self.y *= other;
        self.z *= other;
    }
}

impl<T: Div<Output = T> + Copy> Div<T> for Vec3<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, times: T) -> Self::Output {
        Vec3 {
            x: self.x / times,
            y: self.y / times,
            z: self.z / times,
        }
    }
}

impl<T: DivAssign + Copy> DivAssign<T> for Vec3<T> {
    #[inline(always)]
    fn div_assign(&mut self, times: T) {
        self.x /= times;
        self.y /= times;
        self.z /= times;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn into_iter() {
        let vec = Vec3::new(1, 2, 3);
        let mut iter = vec.into_iter();

        assert_eq!(iter.next(), Some(1));
        assert_eq!(iter.next(), Some(2));
        assert_eq!(iter.next(), Some(3));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn index() {
        let vec = Vec3::new(1, 2, 3);

        assert_eq!(vec[0], 1);
    }

    #[test]
    fn index_mut() {
        let mut vec = Vec3::new(1, 2, 3);
        vec[0] = 2;

        assert_eq!(vec[0], 2);
    }

    #[test]
    fn add() {
        let vec_1 = Vec3::new(1, 2, 3);
        let vec_2 = Vec3::from(2);

        assert_eq!(vec_1 + vec_2, Vec3::new(3, 4, 5));
    }

    #[test]
    fn add_assign() {
        let mut vec_1 = Vec3::new(1, 2, 3);
        let vec_2 = Vec3::from(2);

        vec_1 += vec_2;
        assert_eq!(vec_1, Vec3::new(3, 4, 5));
    }

    #[test]
    fn sub() {
        let vec_1 = Vec3::new(1, 2, 3);
        let vec_2 = Vec3::from(2);

        assert_eq!(vec_1 - vec_2, Vec3::new(-1, 0, 1));
    }

    #[test]
    fn sub_assign() {
        let mut vec_1 = Vec3::new(1, 2, 3);
        let vec_2 = Vec3::from(2);

        vec_1 -= vec_2;
        assert_eq!(vec_1, Vec3::new(-1, 0, 1));
    }

    #[test]
    fn mul() {
        let vec_1 = Vec3::new(1, 2, 3);
        let vec_2 = Vec3::from(2);

        assert_eq!(vec_1 * vec_2, Vec3::new(2, 4, 6));
        assert_eq!(vec_1 * 2, Vec3::new(2, 4, 6));
    }

    #[test]
    fn mul_assign() {
        let mut vec_1 = Vec3::new(1, 2, 3);
        let vec_2 = Vec3::from(2);

        vec_1 *= vec_2;
        assert_eq!(vec_1, Vec3::new(2, 4, 6));

        vec_1 *= 2;
        assert_eq!(vec_1, Vec3::new(4, 8, 12));
    }
}