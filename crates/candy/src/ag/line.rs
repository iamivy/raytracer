use crate::ag::{Point, Vec3};
use std::ops::{Add, Mul};

#[derive(Debug, Clone, Copy, Default)]
pub struct Line<T> {
    pub origin: Point<T>,
    pub direction: Vec3<T>,
}

impl<T: Mul<Output = T> + Add<Output = T> + Copy> Line<T> {
    pub fn new(origin: Point<T>, direction: Vec3<T>) -> Self {
        Self { origin, direction }
    }

    pub fn at(&self, t: T) -> Point<T> {
        self.origin + self.direction * t
    }
}