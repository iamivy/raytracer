pub trait Sqrt {
    fn sqrt(self) -> Self;
}

impl Sqrt for usize {
    fn sqrt(self) -> Self {
        (self as f64).sqrt() as Self
    }
}

impl Sqrt for u128 {
    fn sqrt(self) -> Self {
        (self as f64).sqrt() as Self
    }
}

impl Sqrt for u64 {
    fn sqrt(self) -> Self {
        (self as f64).sqrt() as Self
    }
}

impl Sqrt for u32 {
    fn sqrt(self) -> Self {
        (self as f64).sqrt() as Self
    }
}

impl Sqrt for u8 {
    fn sqrt(self) -> Self {
        (self as f64).sqrt() as Self
    }
}

impl Sqrt for isize {
    fn sqrt(self) -> Self {
        (self as f64).sqrt() as Self
    }
}

impl Sqrt for i128 {
    fn sqrt(self) -> Self {
        (self as f64).sqrt() as Self
    }
}

impl Sqrt for i64 {
    fn sqrt(self) -> Self {
        (self as f64).sqrt() as Self
    }
}

impl Sqrt for i32 {
    fn sqrt(self) -> Self {
        (self as f64).sqrt() as Self
    }
}

impl Sqrt for i8 {
    fn sqrt(self) -> Self {
        (self as f64).sqrt() as Self
    }
}

impl Sqrt for f32 {
    fn sqrt(self) -> Self {
        self.sqrt()
    }
}

impl Sqrt for f64 {
    fn sqrt(self) -> Self {
        self.sqrt()
    }
}