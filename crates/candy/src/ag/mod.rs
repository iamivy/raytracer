pub mod vec_3;
pub mod veg_4;
pub mod line;

mod sqrt;

pub use self::vec_3::Vec3;
pub use self::line::Line;

pub type Point<T> = Vec3<T>;