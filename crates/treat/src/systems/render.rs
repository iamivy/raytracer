use super::Hittable;
use crate::components::{Camera, Texture, Transform};
use crate::{random, Color, Ray};
use image::{ImageResult, Rgb, RgbImage};
use sweets::entity::Entity;
use sweets::id::ComponentIdType;
use sweets::system::System;
use sweets::World;

// use rayon::prelude::*;

#[derive(Clone)]
pub struct Render {
    pub buffer: RgbImage,
    pub camera: Entity,
    pub background: Entity,
    pub samples: usize,
    pub depth: usize,
}

impl Render {
    pub fn new(
        buffer: RgbImage,
        camera: Entity,
        background: Entity,
        samples: usize,
        depth: usize,
    ) -> Self {
        Self {
            buffer,
            camera,
            background,
            depth,
            samples,
        }
    }
}

impl System for Render {
    type Output = ImageResult<()>;

    fn update(&mut self, world: &mut World, id: ComponentIdType) -> Self::Output {
        let width = self.buffer.width();
        let height = self.buffer.height();
        let origin = world
            .get_component::<Transform>(&self.camera)
            .unwrap()
            .position;

        // (0..height).into_par_iter().for_each(|mut h| {
        //     h = height - h;
        //     println!("Current Scanline: {h}");
        //     (0..width).into_iter().for_each(|w| {
        //         let mut color = Color::default();
        //         let mut world = world.clone();
        //         let camera = camera.clone();
        //         let mut buffer = buffer.clone();

        //         for _ in 0..samples {
        //             let u = (w as f64 + random(0.0..1.0)) / (width as f64 - 1.0);
        //             let v = (h as f64 + random(0.0..1.0)) / (height as f64 - 1.0);

        //             let ray = camera.get_ray(origin, u, v);

        //             color += get_color(&ray, &background, &mut world, id, depth);
        //         }

        //         buffer.put_pixel(h, w, color_to_rgb(color, samples));
        //     });
        // });

        let mut last_h = 0;

        self.buffer
            .enumerate_pixels_mut()
            .for_each(|(w, mut h, pixel)| {
                h = height - h;
                if last_h != h {
                    last_h = h;
                    println!()
                }
                print!("\rCurrent Scanline: {h}");
                let mut color = Color::default();

                for _ in 0..self.samples {
                    let u = (w as f64 + random(0.0..1.0)) / (width as f64 - 1.0);
                    let v = (h as f64 + random(0.0..1.0)) / (height as f64 - 1.0);

                    let ray = world
                        .get_component::<Camera>(&self.camera)
                        .unwrap()
                        .get_ray(origin, u, v);

                    color += get_color(&ray, &self.background, world, id, self.depth);
                }

                *pixel = color_to_rgb(color, self.samples);
            });

        // Save Image
        self.buffer.save("out/render.png")?;
        Ok(())
    }
}

fn get_color(
    ray: &Ray,
    background: &Entity,
    world: &mut sweets::World,
    id: ComponentIdType,
    depth: usize,
) -> Color {
    if depth == 0 {
        return Color::default();
    }

    let t_min = 1e-6;
    let t_max = std::f64::INFINITY;
    let mut hittable = Hittable::new(ray, t_min, t_max);
    match hittable.update(world, id) {
        None => {
            return world
                .get_component::<Texture>(background)
                .unwrap()
                .value(ray.direction.unit());
        }
        Some(rec) => {
            let emitted = rec.material.emitted(rec.p);

            match rec.material.scatter(ray, &rec) {
                None => emitted,
                Some((attenuation, scattered)) => {
                    get_color(&scattered, background, world, id, depth - 1) * attenuation
                }
            }
        }
    }
}

#[inline(always)]
fn color_to_rgb(color: Color, samples: usize) -> Rgb<u8> {
    let scale = 1.0 / samples as f64;

    let r = (color.x * scale).sqrt();
    let g = (color.y * scale).sqrt();
    let b = (color.z * scale).sqrt();

    Rgb([
        (256.0 * r.clamp(0.0, 0.999)) as u8,
        (256.0 * g.clamp(0.0, 0.999)) as u8,
        (256.0 * b.clamp(0.0, 0.999)) as u8,
    ])
}
