use crate::components::{Material, Shape, Transform};
use crate::hitrecord::HitRecord;
use crate::Ray;
use sweets::id::ComponentIdType;
use sweets::system::System;

pub struct Hittable<'a> {
    pub ray: &'a Ray,
    pub t_min: f64,
    pub t_max: f64,
}

impl<'a> Hittable<'a> {
    pub fn new(ray: &'a Ray, t_min: f64, t_max: f64) -> Self {
        Self { ray, t_min, t_max }
    }
}

impl System for Hittable<'_> {
    type Output = Option<HitRecord>;

    fn update(&mut self, world: &mut sweets::World, id: ComponentIdType) -> Self::Output {
        let query = world.query_id(id);
        let mut rec = None;
        let mut closet_so_far = self.t_max;

        query.iter().for_each(|entity| {
            let transform = *world.get_component::<Transform>(entity).unwrap();
            match world.get_component::<Shape>(entity).unwrap().hit(
                &transform,
                self.ray,
                self.t_min,
                closet_so_far,
            ) {
                None => {}
                Some(mut temp_rec) => {
                    closet_so_far = temp_rec.t;
                    temp_rec.material = world.get_component::<Material>(entity).unwrap().clone();
                    rec = Some(temp_rec);
                }
            }
        });

        rec
    }
}
