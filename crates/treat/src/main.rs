use std::time::Instant;

use candy::ag::{Point, Vec3};
use image::RgbImage;
use sweets::system::System;
use sweets::World;
use treat::components::{Camera, Material, Shape, Texture, Transform};
use treat::systems::Render;
use treat::Color;

fn main() {
    // time
    let start_time = Instant::now();

    // Image
    let aspect_ratio = 16.0 / 9.0;
    let width = 400;
    let height = (width as f64 / aspect_ratio) as u32;
    let buffer = RgbImage::new(width, height);

    // World initialization
    let mut world = World::default();

    // let sphere = world.create_entity();
    // *world.add_component::<Shape>(&sphere).unwrap() = Shape::SphereR { radius: 2.0 };
    // *world.add_component::<Transform>(&sphere).unwrap() = Transform {
    //     position: Point::new(0.0, 2.0, 0.0),
    //     scale: Vec3::new(2.0, 1.0, 0.5),
    // };
    // *world.add_component::<Material>(&sphere).unwrap() = Material::Emmisive {
    //     texture: Texture::Solid {
    //         color: Color::from(4.0),
    //     }
    // };

    // let sphere = world.create_entity();
    // *world.add_component::<Shape>(&sphere).unwrap() = Shape::SphereR { radius: 2.0 };
    // *world.add_component::<Transform>(&sphere).unwrap() = Transform {
    //     position: Point::new(0.0, 0.0, 0.0),
    //     scale: Vec3::from(1.0),
    // };
    // *world.add_component::<Material>(&sphere).unwrap() = Material::Lambertian {
    //     texture: Texture::Solid {
    //         color: Color::new(0.9, 0.2, 0.3),
    //     }
    // };

    for x in -5..5 {
        for y in -3..3 {
            let sphere = world.create_entity();
            *world.add_component::<Shape>(&sphere).unwrap() = Shape::SphereR { radius: 1.0 };
            world.add_component::<Transform>(&sphere).unwrap().position =
                Point::new(x as f64 * 3.5, y as f64 * 2.5 + 1.25, (x + y) as f64);
            *world.add_component::<Material>(&sphere).unwrap() = Material::Lambertian {
                texture: Texture::Solid {
                    color: Color::new(0.9, 0.2, 0.3),
                },
            }
        }
    }

    // let ground = world.create_entity();
    // let size = 1.0;
    // *world.add_component::<Shape>(&ground).unwrap() = Shape::SphereR { radius: 100.0 };
    // world.add_component::<Transform>(&ground).unwrap().position = Point::new(0.0, -100.0, 0.0);
    // *world.add_component::<Material>(&ground).unwrap() = Material::Lambertian {
    //     texture: Texture::Checker {
    //         odd: Box::new(Texture::Solid {
    //             color: Color::new(0.2, 0.3, 0.1),
    //         }),
    //         even: Box::new(Texture::Solid {
    //             color: Color::from(0.9),
    //         }),
    //         // even: Box::new(Texture::Checker {
    //         //     odd: Box::new(Texture::Solid {
    //         //         color: Color::new(0.1, 0.8, 0.9),
    //         //     }),
    //         //     even: Box::new(Texture::Solid {
    //         //         color: Color::new(0.5, 1.0, 0.2),
    //         //     }),
    //         //     size: size * 2.0,
    //         // }),
    //         size,
    //     },
    // };

    // Camera
    let camera = world.create_entity();
    world.add_component::<Transform>(&camera).unwrap().position = Point::new(0.0, 0.0, -50.0);

    let lookfrom = world.get_component::<Transform>(&camera).unwrap().position;
    let lookat = Vec3::new(0.0, 0.0, 0.0);
    let vup = Vec3::new(0.0, 1.0, 0.0);
    let vfov = 20.0;
    let aperture = 0.1;
    let focus_dist = (lookfrom - lookat).len();

    *world.add_component::<Camera>(&camera).unwrap() = Camera::new(
        lookfrom,
        lookat,
        vup,
        vfov,
        aspect_ratio,
        aperture,
        focus_dist,
    );

    let background = world.create_entity();
    *world.add_component::<Texture>(&background).unwrap() = Texture::Gradient {
        from: Color::from(1.0),
        to: Color::new(0.5, 0.7, 1.0),
    };
    // *world.add_component::<Texture>(&background).unwrap() = Texture::Solid { color: Color::default() };

    let world_time = Instant::now();

    // Render
    let id = world.get_id_of_component::<Shape>()
        | world.get_id_of_component::<Transform>()
        | world.get_id_of_component::<Material>();

    let depth = 500;
    let samples = 100;

    let mut render = Render::new(buffer, camera, background, samples, depth);
    render.update(&mut world, id).unwrap();

    let render_time = Instant::now();

    // Total time
    println!(
        "\n\nWorld time:\t{:?}\nRender time:\t{:?}\nTotal time:\t{:?}",
        world_time.duration_since(start_time),
        render_time.duration_since(world_time),
        render_time.duration_since(start_time)
    );
}
