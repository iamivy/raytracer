use candy::ag::{Line, Vec3};
use rand::Rng;
use std::ops::Range;

pub mod components;
pub mod hitrecord;
pub mod systems;

pub use self::hitrecord::HitRecord;

pub type Ray = Line<f64>;
pub type Color = Vec3<f64>;

pub fn random_in_unit_sphere() -> Vec3<f64> {
    loop {
        let p = Vec3::rand(-1.0..1.0);

        if p.len_squared() >= 1.0 {
            continue;
        }

        return p;
    }
}

pub fn random_unit_vector() -> Vec3<f64> {
    random_in_unit_sphere().unit()
}

pub fn random_in_unit_disk() -> Vec3<f64> {
    loop {
        let mut p = Vec3::rand(-1.0..1.0);
        p.z = 0.0;

        if p.len_squared() >= 1.0 {
            continue;
        }
        return p;
    }
}

pub fn random(range: Range<f64>) -> f64 {
    let mut rng = rand::thread_rng();
    rng.gen_range(range)
}
