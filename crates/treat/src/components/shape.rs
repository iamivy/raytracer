use crate::{HitRecord, Ray};
use candy::ag::Vec3;

use super::Transform;

#[derive(Clone)]
pub enum Shape {
    SphereR { radius: f64 },
    Sphere,
    Rectangle { width: f64, height: f64 },
}

impl Default for Shape {
    fn default() -> Self {
        Self::SphereR {
            radius: Default::default(),
        }
    }
}

impl Shape {
    pub fn hit(
        &mut self,
        transform: &Transform,
        ray: &Ray,
        t_min: f64,
        t_max: f64,
    ) -> Option<HitRecord> {
        match self {
            Shape::SphereR { radius } => {
                let center = transform.position;
                let radius = *radius;

                let oc = ray.origin - center;
                let a = ray.direction.len_squared();
                let half_b = Vec3::dot(oc, ray.direction);
                let c = oc.len_squared() - radius * radius;

                let discriminant = half_b * half_b - a * c;

                if discriminant < 0.0 {
                    return None;
                }

                let sqrtd = discriminant.sqrt();
                let mut root = (-half_b - sqrtd) / a;

                if root < t_min || t_max < root {
                    root = (-half_b + sqrtd) / a;

                    if root < t_min || t_max < root {
                        return None;
                    }
                }

                let mut rec = HitRecord::default();
                rec.t = root;
                rec.p = ray.at(rec.t);
                let outward_normal = (rec.p - center).unit();
                rec.set_face_normal(ray, outward_normal);

                Some(rec)
            }
            Shape::Sphere => {
                let o = transform.position - ray.origin;
                let s = transform.scale;
                let d = ray.direction;

                let pow_s = s * s;
                let pow_o = o * o;
                let pow_d = d * d;

                let a = pow_d.x / pow_s.x + pow_d.y / pow_s.y + pow_d.z / pow_s.z;
                let mut p = (o.x * d.x) / pow_s.x + (o.y * d.y) / pow_s.y + (o.z * d.z) / pow_s.z;
                p /= a;
                let mut q = pow_o.x / pow_s.x + pow_o.y / pow_s.y + pow_o.z / pow_s.z;
                q /= a;

                let in_sqrt = p * p - q + 1.0 / a;
                // dbg!(&in_sqrt);

                if in_sqrt < 0.0 {
                    return None;
                }

                // println!("after sqrt");

                let t_1 = -p + in_sqrt.sqrt();
                let t_2 = -p - in_sqrt.sqrt();

                // Should be t_1 < t_2
                let t = if t_1 < t_2 { t_1 } else { t_2 };

                // println!("Before t_min");

                if t_min > t || t_max < t {
                    return None;
                }

                let p = ray.at(t);

                let mut rec = HitRecord {
                    t,
                    p,
                    ..Default::default()
                };

                let outward_normal = (p - transform.position).unit();
                rec.set_face_normal(ray, outward_normal);
                // dbg!(&rec);
                // println!("hit");
                Some(rec)
            }
            Shape::Rectangle { width, height } => {
                let (width, height) = (*width, *height);
                let center = transform.position;
                let scale = transform.scale;

                let t = (center.z - ray.origin.z) / ray.direction.z;
                if t < t_min || t > t_max {
                    return None;
                }

                let x = ray.origin.x + t * ray.direction.x;
                let y = ray.origin.y + t * ray.direction.y;

                let x0 = center.x - width * 0.5 * scale.x;
                let x1 = center.x + width * 0.5 * scale.x;
                let y0 = center.y - height * 0.5 * scale.y;
                let y1 = center.y + height * 0.5 * scale.y;

                if x < x0 || x > x1 || y < y0 || y > y1 {
                    return None;
                }

                let mut rec = HitRecord {
                    t,
                    p: ray.at(t),
                    ..Default::default()
                };
                let outward_normal = Vec3::new(0.0, 0.0, 1.0);
                rec.set_face_normal(ray, outward_normal);

                Some(rec)
            }
        }
    }
}
