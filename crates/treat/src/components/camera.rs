use crate::{random_in_unit_disk, Ray};
use candy::ag::{Point, Vec3};

#[derive(Default, Clone)]
pub struct Camera {
    pub horizontal: Vec3<f64>,
    pub vertical: Vec3<f64>,
    pub lower_left_corner: Point<f64>,
    pub lens_radius: f64,
    pub u: Vec3<f64>,
    pub v: Vec3<f64>,
}

impl Camera {
    pub fn new(
        lookfrom: Point<f64>,
        lookat: Point<f64>,
        vup: Vec3<f64>,
        vfov: f64,
        aspect_ratio: f64,
        aperture: f64,
        focus_dist: f64,
    ) -> Self {
        let theta = vfov * std::f64::consts::PI / 180.0;
        let h = (theta * 0.5).tan();
        let viewport_height = 2.0 * h;
        let viewport_width = aspect_ratio * viewport_height;

        let w = (lookfrom - lookat).unit();
        let u = (Vec3::cross(vup, w)).unit();
        let v = Vec3::cross(w, u);

        let horizontal = u * viewport_width * focus_dist;
        let vertical = v * viewport_height * focus_dist;
        let lower_left_corner = lookfrom - horizontal * 0.5 - vertical * 0.5 - w * focus_dist;

        let lens_radius = aperture * 0.5;

        Self {
            horizontal,
            vertical,
            lower_left_corner,
            lens_radius,
            u,
            v,
        }
    }

    pub fn get_ray(&self, origin: Point<f64>, u: f64, v: f64) -> Ray {
        let rd = random_in_unit_disk() * self.lens_radius;
        let offset = self.u * rd.x + self.v * rd.y;

        Ray::new(
            origin + offset,
            self.lower_left_corner + self.horizontal * u + self.vertical * v - origin - offset,
        )
    }
}
