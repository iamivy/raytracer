use crate::{
    components::Texture, random, random_in_unit_sphere, random_unit_vector, Color, HitRecord, Ray,
};
use candy::ag::{Point, Vec3};

#[derive(Debug, Clone, PartialEq)]
pub enum Material {
    Lambertian { texture: Texture },
    Metal { texture: Texture, fuzz: f64 },
    Dielectric { ref_index: f64 },
    Emmisive { texture: Texture },
}

impl Default for Material {
    fn default() -> Self {
        Self::Lambertian {
            texture: Texture::default(),
        }
    }
}

impl Material {
    pub fn emitted(&self, p: Point<f64>) -> Color {
        match self {
            Material::Emmisive { texture } => texture.value(p),
            _ => Color::default(),
        }
    }

    pub fn scatter(&self, ray: &Ray, rec: &HitRecord) -> Option<(Color, Ray)> {
        let (scattered, attenuation);
        match self {
            Material::Lambertian { texture } => {
                let mut scatter_direction = rec.normal + random_unit_vector();

                if scatter_direction.near_zero() {
                    scatter_direction = rec.normal;
                }

                scattered = Ray::new(rec.p, scatter_direction);
                attenuation = texture.value(rec.p);
                Some((attenuation, scattered))
            }
            Material::Metal { texture, fuzz } => {
                let reflected = reflect(ray.direction.unit(), rec.normal);
                scattered = Ray::new(rec.p, reflected + random_in_unit_sphere() * *fuzz);
                attenuation = texture.value(rec.p);
                if Vec3::dot(scattered.direction, rec.normal) <= 0.0 {
                    return None;
                }

                Some((attenuation, scattered))
            }
            Material::Dielectric { ref_index } => {
                let refraction_ratio = if rec.front_face {
                    1.0 / ref_index
                } else {
                    *ref_index
                };

                let unit_dir = ray.direction.unit();
                let cos_theta = f64::min(Vec3::dot(-unit_dir, rec.normal), 1.0);
                let sin_theta = (1.0 - cos_theta * cos_theta).sqrt();

                let cannot_refract = refraction_ratio * sin_theta > 1.0;

                let direction = if cannot_refract
                    || reflectance(cos_theta, refraction_ratio) > random(0.0..1.0)
                {
                    reflect(unit_dir, rec.normal)
                } else {
                    refract(unit_dir, rec.normal, refraction_ratio)
                };

                scattered = Ray::new(rec.p, direction);
                attenuation = Vec3::from(1.0);

                Some((attenuation, scattered))
            }
            Material::Emmisive { texture: _ } => None,
        }
    }
}

fn reflect(v: Vec3<f64>, n: Vec3<f64>) -> Vec3<f64> {
    v - n * Vec3::dot(v, n) * 2.0
}

fn refract(uv: Vec3<f64>, n: Vec3<f64>, etai_over_etat: f64) -> Vec3<f64> {
    let cos_theta = f64::min(Vec3::dot(-uv, n), 1.0);
    let r_out_perp = (uv + n * cos_theta) * etai_over_etat;
    let r_out_parallel = n * -f64::abs(1.0 - r_out_perp.len_squared()).sqrt();
    r_out_perp + r_out_parallel
}

fn reflectance(cosine: f64, ref_index: f64) -> f64 {
    let mut r_0 = (1.0 - ref_index) / (1.0 + ref_index);
    r_0 *= r_0;
    r_0 + (1.0 - r_0) * f64::powf(1.0 - cosine, 5.0)
}
