use crate::Color;
use candy::ag::Point;

#[derive(Debug, Clone, PartialEq)]
pub enum Texture {
    Solid {
        color: Color,
    },
    Checker {
        odd: Box<Texture>,
        even: Box<Texture>,
        size: f64,
    },
    Gradient {
        from: Color,
        to: Color,
    },
}

impl Default for Texture {
    fn default() -> Self {
        Texture::Solid {
            color: Color::default(),
        }
    }
}

impl Texture {
    pub fn value(&self, p: Point<f64>) -> Color {
        match self {
            Self::Checker { odd, even, size } => {
                let sines = (size * p.x).sin() * (size * p.y).sin() * (size * p.z).sin();

                if sines < 0.0 {
                    odd.value(p)
                } else {
                    even.value(p)
                }
            }
            Self::Solid { color } => *color,
            Self::Gradient { from, to } => {
                // let unit_dir = ray.direction.unit();
                // let t = 0.5 * (unit_dir.y + 1.0);
                // Color::from(1.0) * (1.0 - t) + Color::new(0.5, 0.7, 1.0) * t

                let t = 0.5 * (p.y + 1.0);
                *from * (1.0 - t) + *to * t
            }
        }
    }
}
