use candy::ag::{Point, Vec3};

#[derive(Default, Debug, Clone, Copy)]
pub struct Transform {
    pub position: Point<f64>,
    pub scale: Vec3<f64>,
}

impl Transform {}
