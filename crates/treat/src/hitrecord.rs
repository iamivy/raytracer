use crate::{components::Material, Ray};
use candy::ag::{Point, Vec3};

#[derive(Default, Debug)]
pub struct HitRecord {
    pub t: f64,
    pub p: Point<f64>,
    pub normal: Vec3<f64>,
    pub material: Material,
    pub front_face: bool,
}

impl HitRecord {
    pub fn set_face_normal(&mut self, ray: &Ray, outward_normal: Vec3<f64>) {
        self.front_face = Vec3::dot(ray.direction, outward_normal) < 0.0;
        self.normal = if self.front_face {
            outward_normal
        } else {
            -outward_normal
        }
    }
}
