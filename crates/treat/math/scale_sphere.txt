Scaling a Sphere in 3 Dimensionen.

Gegeben ist ein dreidimensionales Koordinatensystem, in welchem sich eine Kugel befindet,
von welcher wir ihren Mittelpunkt und den Radius wissen. Des Weiteren haben wir eine Kamera
in diesem System, welche ebenfalls einen Ursprung (Punkt) hat. Von diesem Ursprung aus wird
eine Gerade durch ein Pixel vor der Kamera geschickt. Um zu überprüfen, ob die Gerade die
Kugel trifft, kann man wie folgt vorgehen: Die Gleichung für eine Kugel, zentriert um einen
Mittelpunkt mit einem Radius r

x² + y² + z² = r² um zu überprüfen, ob ein Punkt auf der Oberfläche ist,
muss x² + y² + z² = r² gelten. Um zu überprüfen, ob ein Punkt in der Kugel ist
gilt: x² + y² + z² < r² und damit ein Punkt außerhalb der Kugel ist
gilt x² + y² + z² > r² wenn wir nun den Mittelpunkt auch als

Punkt (C_x, C_y, C_z) betrachten, sieht das ganze wie folgt aus
(x - C_x)² + (y - C_y)² + (z - C_z)² = r²

ausgedrückt in Vektoren, wenn C = (C_x, C_y, C_z) und P (x, y, z) ist die Gleichung wie folgt

(P - C) * (P - C) = r² dies lässt sich lesen als, "jeder Punkt P welcher diese Gleichung erfüllt,
ist auf der Kugel". Wir wollen wissen, ob die

Gerade P(t) = A + tB trifft die Kugel irgendwo. Wenn dies der Fall ist gibt es ein t welches
für P(t) die Gleichung erfüllt. Dementsprechend suchen wir jedes t wo das der Fall ist.

(P(t) - C) * (P(t) - C) = r² oder ausgeschrieben (A + tB - C) * (A + tB - C) = r²
formulieren wir nun diesen Term aus und verschieben alles auf eine Seite bekommen
wir
t² * B² + 2tb * (A - C) + (A - C) * (A - C) - r² = 0
Alle Vektoren und r sind bekannt.
Nur t ist unbekannt und die Gleichung ist eine Quadratische. Wir können diese nach t auflösen
und bekommen entweder keine Lösung, eine oder 2 Eine bedeutet wir treffen die Kugel an genau
einer Stelle, 2 bedeutet, dass die Gerade durch die Kugel geht und 0 dass er dran vorbeigeht.
So dieses Problem habe ich gelöst, sprich ich weiß, wann ich die Kugel treffe. Soeben möchte
ich dir Kugel aber auch Skalen können. Wenn ich die Kugel gesamt scalen möchte, kann ich
einfach den Radius mit dem Scale wert multiplizieren. Allerdings möchte ich in alle Richtungen
Skalen, sprich ich habe zusätzlich zu allem noch einen Vektor mit drei werten, für die Skalierung in
die x, y, und z Richtung.

HOW?????

r = s_x (wenn eine Kugel!)
ich brauche kein Radius sondern nur scale!

(P_x - C_x)² / (S_x)² + (P_y - C_y)² / S_y² + (P_z - C_z)² / S_z² <= 1
Alle auf und in der Elipsiuid

C = Mittelpunkt
S = Skalierung
P = Punkt, welcher den Term erfüllt liegt auf der Elpise

t = the point
d = direction
o = origin
c = center

t² + d² + 2td * (o - c) + (o - c) * (o - c) - r² = 0

oc = (o - c)

t² + d² + 2td * oc + oc * co - r² = 0

a = d² = dot(d, d)

t² + a + 2td * oc + oc² - r² = 0

c = oc² - r² = dot(oc, oc) - r²

t² + a + 2td * oc + c = 0		| - t²
a + 2td + oc + c = -t²

b = 2td * oc = 2 * dot(oc, d)

b² - 4 * a * c > 0.0

2td * oc - 4 * d² * oc² - r² > 0.0 = Sphere has been hit

(p - c)² / s²

g: x = o + td

r_x: (p_x - c_x)² / s_x²
r_y: (p_y - c_y)² / s_y²
r_z: (p_z - c_z)² / s_z²

p = g: x = o + td
r_x: (p - c_x)² / s_x²
r_y: (p - c_y)² / s_y²
r_z: (p - c_z)² / s_z²

r_x: (o_x + t * d_x - c_x)² / s_x²
r_y: (o_y + t * d_y - c_y)² / s_y²
r_z: (o_z + t * d_z - c_z)² / s_z² 

  I: (o_x + t * d_x - c_x)² / s_x²
 II: (o_y + t * d_y - c_y)² / s_y²
III: (o_z + t * d_z - c_z)² / s_z²

r_x + r_y + r_z <= 1

I + II + III <= 1

(o + t * d - c)² / s² <= 1
(o + t * d - c) * (o + t * d - c) / s² <= 1
o² + tdo - oc + tdo + t²d² - tdc - oc - tdc + c² / s² <= 1
o² + 2tdo - 2oc + (td)² - 2tdc + c² / s² <= 1

a = c² / s²
b = o² + c²
f = 2oc

b + a - f + 2tdo + (td)² - 2tdc <= 1

k = b + a - f

k + 2tdo + (td)² - 2tdc <= 1 | 1 - k
2tdo + (td)² - 2tdc <= 1 - k

let q = 2do
let z = 2dc

tq + (td)² - tz <= 1 - k		| / d²

tq / d² + t² - tz / d² = (1 - k) / d²

v = q / d²
w = z / d²
h = (1 - k) / d²

tv + t² - tw = h
t² + tv - tw = h

p = (v - w)
q = -h

t² + t * (v - w) - h = 0

t² + tp + q = 0

- p / 2 +- sqrt((p / 2)² - q)

x² + px + q = 0
- p / 2 +- sqrt((p / 2)² - q)