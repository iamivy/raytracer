use crate::id::ComponentIdType;
use crate::World;

pub trait System {
    type Output;

    fn update(&mut self, world: &mut World, id: ComponentIdType) -> Self::Output;
}
