use libc::{free, malloc, realloc};
use std::mem::size_of;

/// A ComponentPool's job is, to store the data of the actuall components.
/// The struct contains the `pool` it self, a pointer to the first byte of the pool
/// to be percise. The `size` is set when `new::<Component>()` is called and resembels
/// the size of the components. `count` keeps tract of the amount of stored components.
/// `capacity` keeps tract on the amount of allocated memory. To understand `entiteis`
/// and `free_indices` the idea of the pool needs to be clear. Each `Entity` can have
/// one instance of every component. To get this component the function `aquire()` is
/// called. If a `Entity` for example dies we don't need to store the component any
/// more. With that in mind we have to options:
///     1. Destroy the component, live with the now unused allocated space and create
///        a new one when needed.
///     2. Leave the component be, push the idx of the component into a `free_indicies`
///        and the next time, we need a new component we canm check if there are any
///        indicies in `free_indicies` and if so, we can reuse this component.
///
/// Last but not least we have the `entities` vector. This vector holds the `pool`
/// indices for each currusponding `Entity`. An index of 0 means, that this `Entity`
/// doesn't have a component. Having this `Vec` makes the lookups way simpler.
#[derive(Debug)]
pub struct ComponentPool {
    /// The pointer to the start of the pool.
    pool: *const u8,
    /// The size of each component.
    size: usize,
    /// The amount of components aquired already.
    count: usize,
    /// The amount of allocated memory for the pool.
    capacity: usize,
    /// The indicies of each `entity`. 0 means no component for the `Entity`. As 0 is also
    /// a valid index, we need to offset the saved indexes so 0 is not used. Offsetting by 1
    /// is enough in this case. Yet this also means that we have to subtract 1 of the saved
    /// index to get the real one.
    entities: Vec<usize>,
    /// A list of free_indices so that components can be reused.
    free_indices: Vec<usize>,
}

impl ComponentPool {
    /// `new<T>()` initializes the pool and sets the size of the future components T,
    /// so that we can allocate the right amount of memory. Returns an Instance of Pool.
    pub fn new<T>() -> Self {
        let size: usize = size_of::<T>();
        let capacity = 2;
        Self {
            pool: unsafe { malloc(capacity * size).cast() },
            size,
            count: 0,
            capacity,
            entities: vec![],
            free_indices: vec![],
        }
    }

    /// `aquire<T>()` returns a mutuable reference to the corrusponding component T for the given Entity.
    /// If a component has already been registered for the Entity, this component will be returned. Otherwise
    /// a new component will be created, with default parameters. If a new component needs to be created
    /// we first check, if we can reuse any of the old components. If now, we check if we have enough
    /// allocated memory for a new component and depending on the outcome we allocate twice the amount we
    /// currently occupie. Afterwards we will create a new component to return.
    pub fn aquire<T: Default>(&mut self, edx: usize) -> &mut T {
        if edx >= self.entities.len() {
            self.entities.resize(edx + 1, 0);
        }

        let idx = self.entities[edx];
        if idx != 0 {
            // self.pool is a pointer to the start of the
            // ComponentPool. Adding our index times the size
            // of T gives us the desired Component.
            // idx - 1 as 0 is used for None. As 0 is still a
            // valid index we need to use the idx - 1 as it is
            // the valid index for the pool.
            unsafe {
                self.pool
                    .add((idx - 1) * self.size)
                    .cast_mut() // make mutable
                    .cast::<T>() // cast to expected type
                    .as_mut() // get the mutable reference
                    .unwrap()
            } // we know that the value is valid when the index is found
        } else {
            // create new component
            if let Some(idx) = self.free_indices.pop() {
                // check if there is a index to be
                // reused
                self.entities[edx] = idx + 1; // reuse the index

                let ptr = self.aquire(edx); // get the component
                *ptr = T::default(); // reset it
                return ptr; // return
            }

            if self.count >= self.capacity {
                // check if the pool has enough
                // space
                // grow array
                self.capacity *= 2; // doubel it
                unsafe {
                    self.pool = realloc(
                        // reallocate memory to fit double
                        self.pool.cast_mut().cast(), // the size
                        self.capacity * self.size,
                    )
                    .cast();
                }
            }

            self.entities[edx] = self.count + 1; // Add one for the offset so 0 is None.
            self.count += 1; // advance the count

            let ptr = self.aquire(edx);
            *ptr = T::default();
            ptr
        }
    }

    /// If a component isn't needed we can release it back. For this
    /// we push the index corrusponding with the component to the
    /// `free_indicies` vector to be reused another time.
    pub fn release(&mut self, edx: usize) {
        if self.entities.len() < edx {
            return;
        } // entity has never been created with this component
        let idx = self.entities[edx];
        if idx == 0 {
            return;
        }
        self.entities[edx] = 0;
        self.free_indices.push(idx - 1);
    }
}

impl Drop for ComponentPool {
    fn drop(&mut self) {
        unsafe {
            free(self.pool.cast_mut().cast());
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[derive(Debug, Default, PartialEq, Clone)]
    struct Transform {
        x: f32,
        y: f32,
        z: f32,
    }

    impl From<f32> for Transform {
        fn from(value: f32) -> Self {
            Self {
                x: value,
                y: value,
                z: value,
            }
        }
    }

    #[test]
    fn aquire_new() {
        let mut pool = ComponentPool::new::<Transform>();
        let capacity = pool.capacity;

        pool.aquire::<Transform>(0);
        pool.aquire::<Transform>(1);
        let t_2 = pool.aquire::<Transform>(2);

        *t_2 = Transform::from(1.0);
        assert_eq!(*t_2, Transform::from(1.0));

        let t_3 = pool.aquire::<Transform>(3);
        assert_eq!(*t_3, Transform::default());

        if capacity != 2 {
            return;
        }
        assert_eq!(pool.capacity, 4);
    }

    #[test]
    fn aquire_a_lot() {
        let mut pool = ComponentPool::new::<Transform>();
        for idx in 1..1_000_000 {
            pool.aquire::<Transform>(idx);
            if idx % 2 == 0 {
                pool.release(idx - 2);
            }
            if idx % 3 == 0 {
                pool.release(idx - 3);
            }
            if idx % 7 == 0 {
                pool.release(idx - 7);
            }
        }
    }

    #[test]
    fn aquire_reused() {
        let mut pool = ComponentPool::new::<Transform>();
        let capacity = pool.capacity;

        let t_0 = pool.aquire::<Transform>(0);
        *t_0 = Transform {
            x: 10.0,
            ..Default::default()
        };
        pool.release(0);
        assert_eq!(pool.free_indices.len(), 1);

        let t_1 = pool.aquire::<Transform>(1);
        assert_eq!(*t_1, Transform::default());

        if capacity != 2 {
            return;
        }
        assert_eq!(pool.capacity, 2);
    }
}
