use crate::entity::Entity;
use crate::component_pool::ComponentPool;
use crate::id::{GenerationType, IndexType, ComponentIdType};
use fxhash::FxHashMap;
use std::any::TypeId;
use std::mem::transmute;

#[derive(Default)]
pub struct World {
	generation: Vec<GenerationType>,
	free_indicies: Vec<usize>,
	components: Vec<ComponentIdType>,
	indices: FxHashMap<u64, usize>,
	queries: Vec<Vec<Entity>>,
	pools: Vec<ComponentPool>,
}

impl World {
	pub fn create_entity(&mut self) -> Entity {
		let idx = if let Some(idx) = self.free_indicies.pop() {
			idx			
		} else {
			self.generation.push(0);
			self.components.push(0);
			self.generation.len() - 1
		};

		Entity {
			index: idx as IndexType,
			generation: self.generation[idx],
		}
	}

	pub fn destroy_entity(&mut self, entity: &Entity) {
		if !self.is_alive(entity) {
			return;
		}

		let idx = entity.index();

		for id in 0..ComponentIdType::BITS as usize {
			if self.has_component_id(entity, 1 << id) {
				self.pools[id].release(idx);
				self.queries[id].retain(|e| e != entity);
			}
		}

		self.generation[idx] += 1;
		self.components[idx] = 0;
		self.free_indicies.push(idx);
	}

	pub fn is_alive(&self, entity: &Entity) -> bool {
		self.generation[entity.index()] == entity.generation
	}

	pub fn query_id(&mut self, id: ComponentIdType) -> Vec<Entity> {
		// For large amount of data it could be usefull to take the smallest vec as the res vec.
		let mut res = vec![];
		if id == 0 { return res; }

		for idx in 0..ComponentIdType::BITS as usize {
			if (id & (1 << idx)) != 0 {
				res = self.queries[idx].clone();
				break;
			}
		}
		res.retain(|e| self.has_component_id(e, id));
		res
	}

	pub fn add_component<T: Default + 'static>(&mut self, entity: &Entity) -> Option<&mut T> {
		if !self.is_alive(entity) { return None; }

		let id = self.get_id_of_component::<T>();
		let idx = self.get_index_of_component::<T>();

		self.add_component_id(entity, id);
		self.queries[idx].push(*entity);

		Some(self.pools[idx].aquire::<T>(entity.index()))
	}

	pub fn get_component<T: Default + 'static>(&mut self, entity: &Entity) -> Option<&mut T> {
		if !self.is_alive(entity) { return None; }

		let id = self.get_id_of_component::<T>();
		let idx = self.get_index_of_component::<T>();

		if !self.has_component_id(entity, id) {
			return None;
		}

		Some(self.pools[idx].aquire::<T>(entity.index()))
	}

	pub fn remove_component<T: Default + 'static>(&mut self, entity: &Entity) {
		if !self.is_alive(entity) { return; }

		let id = self.get_id_of_component::<T>();
		let idx = self.get_index_of_component::<T>();

		self.queries[idx].retain(|e| e != entity);
		self.remove_component_id(entity, id);
		self.pools[idx].release(entity.index());
	}

	pub fn has_component<T: Default + 'static>(&mut self, entity: &Entity) -> bool {
		let id = self.get_id_of_component::<T>();
		self.has_component_id(entity, id)
	}

	fn add_component_id(&mut self, entity: &Entity, id: ComponentIdType) {
		self.components[entity.index()] |= id;
	}

	fn remove_component_id(&mut self, entity: &Entity, id: ComponentIdType) {
		self.components[entity.index()] ^= id;
	}

	pub fn get_component_id(&mut self, entity: &Entity) -> ComponentIdType {
		self.components[entity.index()]
	}

	pub fn has_component_id(&self, entity: &Entity, id: ComponentIdType) -> bool {
		self.components[entity.index()] & id == id
	}

	fn get_index_of_component<T: 'static>(&mut self) -> usize {
		let type_id = unsafe { transmute::<TypeId, u64>(TypeId::of::<T>()) };
		match self.indices.get(&type_id) {
			Some(idx) => *idx,
			None => {
				let idx = self.pools.len();
				self.indices.insert(type_id, idx);
				self.pools.push(ComponentPool::new::<T>());
				self.queries.push(Vec::new());
				idx
			}
		}
	}

	pub fn get_id_of_component<T: 'static>(&mut self) -> ComponentIdType {
		1 << self.get_index_of_component::<T>()
	}
}

#[cfg(test)]
mod tets {
	use super::*;

	#[derive(Default, PartialEq)]
	struct Material {}

	#[derive(Default, PartialEq)]
	struct Transform {
		position: [f64; 3],
	}

	#[derive(Default, PartialEq, Debug)]
	struct Light {}

	#[derive(Default, PartialEq)]
	struct Banana {
		value: f64
	}

	#[test]
	fn is_alive() {
		let mut world = World::default();
		let entity = world.create_entity();
		assert!(world.is_alive(&entity));
		world.destroy_entity(&entity);
		assert!(!world.is_alive(&entity));
	}

	#[test]
	fn get_component() {
		let mut world = World::default();
		let entity = world.create_entity();
		*world.add_component::<Banana>(&entity).unwrap() = Banana { value: 1.0 };
		assert_eq!(world.get_component::<Banana>(&entity).unwrap().value, 1.0);
		assert_eq!(world.get_component::<Light>(&entity), None);
	}

	#[test]
	fn get_add_remove_component_id() {
		let mut world = World::default();
		let entity = world.create_entity();
		assert_eq!(world.get_component_id(&entity), 0);
		world.add_component::<Banana>(&entity);
		assert_eq!(world.get_component_id(&entity), 1);
		world.add_component::<Light>(&entity);
		assert_eq!(world.get_component_id(&entity), 3);
		world.remove_component::<Banana>(&entity);
		assert_eq!(world.get_component_id(&entity), 2);
	}

	#[test]
	fn has_component_id() {
		let mut world = World::default();
		let entity = world.create_entity();
		let id = world.get_id_of_component::<Banana>()
			| world.get_id_of_component::<Light>();
		assert!(!world.has_component_id(&entity, id));
		world.add_component::<Banana>(&entity);
		assert!(!world.has_component_id(&entity, id));
		world.add_component::<Light>(&entity);
		assert!(world.has_component_id(&entity, id));
	}

	#[test]
	fn query_id() {
        let mut world = World::default();
		for i in 0..100_000 {
			let entity = world.create_entity();
			assert_eq!(entity.index, i);
			if i % 2 == 0 { world.add_component::<Banana>(&entity); }
			if i % 5 == 0 { world.add_component::<Transform>(&entity); }
		}

		let query = world.query_id(3);
		query.iter().for_each(|e| {
			assert!(e.index() % 2 == 0);
			assert!(e.index() % 5 == 0);
			assert!(world.has_component::<Banana>(e));
			assert!(world.has_component::<Transform>(e));
		})
	}

	#[test]
	fn get_index_of_component() {
		let mut world = World::default();
		assert_eq!(world.get_index_of_component::<Banana>(), 0);
		assert_eq!(world.get_index_of_component::<Material>(), 1);
		assert_eq!(world.get_index_of_component::<Transform>(), 2);
		assert_eq!(world.get_index_of_component::<Banana>(), 0);
		assert_eq!(world.get_index_of_component::<Light>(), 3);
		assert_eq!(world.get_index_of_component::<Transform>(), 2);
	}

	#[test]
	fn get_id_of_component() {
		let mut world = World::default();
		assert_eq!(world.get_id_of_component::<Banana>(), 1);
		assert_eq!(world.get_id_of_component::<Material>(), 2);
		assert_eq!(world.get_id_of_component::<Transform>(), 4);
		assert_eq!(world.get_id_of_component::<Banana>(), 1);
		assert_eq!(world.get_id_of_component::<Light>(), 8);
		assert_eq!(world.get_id_of_component::<Transform>(), 4);
	}
}