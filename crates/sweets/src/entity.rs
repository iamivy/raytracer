use crate::id::{GenerationType, IndexType};

/// An `Entity` is just an `index` and `generation`. In the case of using
/// 32 Bits for each, we can create over 4.000.000.000 Entitys 4.000.000.000
/// times.
#[derive(Debug, Default, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Entity {
    pub index: IndexType,
    pub generation: GenerationType,
}

impl Entity {
    pub fn new(index: IndexType, generation: GenerationType) -> Self {
        Entity { index, generation }
    }

    #[inline(always)]
    pub fn index(&self) -> usize {
        self.index as usize
    }
}

impl From<IndexType> for Entity {
    fn from(index: IndexType) -> Self {
        Entity {
            index,
            generation: 0,
        }
    }
}
