pub type IndexType = u32;
pub type ComponentIdType = usize;
pub type GenerationType = u32;