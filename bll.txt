Meine Beziehung mit der Informatik

	  I:	Flashback zu WP1

	 II:	Warum einen Pathtracer und ein ECS

	III:	Leitfrage

Warum wir sehen

	  I:	Licht hüpft durch die Gegend

	 II:	Oberflächen Absorbieren und Reflektieren Licht

	III:	Licht fällt in unser Auge

Die Mathematik eines Pathtracer

	  I:	Was sind Strahlen

	 II:	Implementation von Vektoren und Geraden

	
Vorstellung Rust

Objekte

	  I:	Warum überhaupt Kugeln

	 II:	Materialen und Texturen

Die Welt

	  I:	Warum ein ECS

	 II:	Was genau ist eine Entitität

	III:	Was genau ist ein Component

	 VI:	Wie stehen sie in zusammenhang mit einander

	  V:	ComponentPools

	 IV:	Die World

Component Implementationen

	  I:	Transform

	 II:	Shape

	III:	Material und Texture

	 VI:	Render

System Implementationen

	  I:	Hittable

	 II:	Render

Mehr Materialien

	  I:	Metal

	 II:	Glass

Fazit

	  I:	Was ich in einem Jahr geschafft habe

	 II:	Challanges

	III:	Meine Zukunftspläne


	
Titel: Entwicklung eines Raytracers mit Entity Component System und einer Mathe-Bibliothek in Rust

Abstract:
- Kurze Zusammenfassung der Facharbeit, inklusive der Leitfrage, der Methodik, der Ergebnisse und der Schlussfolgerungen.

1. Einleitung
1.1. Hintergrund
- Erläuterung der Bedeutung von Raytracing und Entity Component System (ECS) in der Computergrafik
- Vorstellung der Programmiersprache Rust und ihrer Anwendung in der Systemprogrammierung

1.2. Leitfrage
- Inwiefern ist es möglich, ein Raytracer mit einem eingebundenden Entity Component System und einer Mathe-Bibliothek in Rust zu erstellen?

1.3. Motivation
- Warum diese Fragestellung untersucht wird und welche Vorteile sich daraus ergeben könnten.

1.4. Aufbau der Facharbeit
- Übersicht über die Struktur der Arbeit und die behandelten Themen.

2. Grundlagen
2.1. Raytracing
- Grundkonzept des Raytracing
- Vorteile und Nachteile des Raytracing-Verfahrens

2.2. Entity Component System (ECS)
- Grundkonzept des Entity Component Systems
- Vorteile und Anwendungsbereiche von ECS

2.3. Rust
- Einführung in die Programmiersprache Rust
- Besonderheiten und Vorteile von Rust im Kontext von Systemprogrammierung und Computergrafik

2.4. Mathe-Bibliotheken
- Übersicht über gängige Mathe-Bibliotheken in Rust
- Anforderungen an Mathe-Bibliotheken für Raytracing und ECS

3. Methodik
3.1. Systemarchitektur
- Entwurf der Systemarchitektur für den Raytracer mit ECS und Mathe-Bibliothek in Rust
- Entscheidungen über Systemkomponenten und deren Interaktionen

3.2. Implementierung
- Beschreibung der Umsetzung des Raytracers, der ECS und der Integration der Mathe-Bibliothek
- Herausforderungen und Lösungsansätze

4. Ergebnisse
4.1. Funktionalität
- Validierung der Funktionalität des Raytracers und der ECS
- Beispiele für erstellte Szenen und Grafiken

4.2. Leistung
- Analyse der Leistung des erstellten Raytracers in Bezug auf Geschwindigkeit und Speicherverbrauch
- Vergleich mit anderen Raytracing-Implementierungen

5. Diskussion
5.1. Beantwortung der Leitfrage
- Inwiefern war es möglich, den Raytracer mit einem eingebundenden ECS und einer Mathe-Bibliothek in Rust zu erstellen?

5.2. Limitationen und Verbesserungspotential
- Besprechung der Limitationen der entwickelten Implementierung
- Vorschläge für zukünftige Verbesserungen und Erweiterungen

6. Fazit
- Zusammenfassung der wichtigsten Erkenntnisse aus der Facharbeit
- Schlussfolgerungen bezüglich der Leitfrage und der Umsetzbarkeit des Projekts

7. Literaturverzeichnis
- Liste der verwendeten Quellen und Literatur

8. Anhang
- Quellcode der Implementierung
- Weitere Materialien, die im Kontext der Facharbeit relevant sind
Quellen

	  I:	Programmieren im Allgemeinen 

			a.	strager
				(2023, 11. März)
				Faster than Rust and C++: the PERFECT hash table [Video]
				YouTube
				https://www.youtube.com/watch?v=DMQ_HcNSOAI	
				letzter zugriff: 25.04.2023 - 15:40

				Dieses Video erklärt die Grundlagen von HashMaps bzw. HashTables und wie
				man diese Optimieren kann, wenn man Spezielle Fälle betrchtet.

			b.	Josh’s Channel
				(2023, 28. Februar)
				How Binary Works, and the Power of Abstraction [Video]
				YouTube
				https://www.youtube.com/watch?v=PMpNhbMjDj0
				letzter zugriff: 25.04.2023 - 15:57

	 II:	Rust

			a.	The Rust Programming Language - The Rust Programming Language
				https://doc.rust-lang.org/book/
				letzter zugriff: 25.04.2023 - 15:47

				Das offizielle Rust Buch behandelt jede Fazette der Rust-Programmiersprache und ist ein großartiger
				einstieg in die Sprache.

			b.	Rust-Lang
				GitHub - rust-lang/rustlings: Small exercises to get you used to reading and writing Rust code!
				GitHub
				https://github.com/rust-lang/rustling
				letzter zugriff: 25.04.2023 - 15:40

				Diese Ansammlung an kleinen Problemen hat mir geholfen, die Rust-Programmiersprache zu lernen
				und zu vertiefen, vorallem in Combination mit dem Offiziellen Buch.

			c.	std - Rust
				https://doc.rust-lang.org/std/
				letzter zugriff: 25.04.2023 - 15:47

				Die Dokumentation der Standardbibliothek ist ein großartiger Einsteig, um nachzuschauen, welche
				Methoden für was implementiert sind und wie genau.

	III:	Entity Component System

			a.	Niklas Gray
				(2014, August)
				Building a Data-Oriented Entity System (part 1)
				http://bitsquid.blogspot.com/2014/08/building-data-oriented-entity-system.html
				letzter zugriff: 25.04.2023 - 16:22

			b.	Niklas Gray
				(2014, Septmeber)
				Building a Data-Oriented Entity System (Part 2: Components)
				https://bitsquid.blogspot.ch/2014/09/building-data-oriented-entity-system.html
				letzter zugriff: 25.04.2023 - 16:22

			c.	Mertens, S.
				(2021, 15. Dezember)
				Why Vanilla ECS Is Not Enough - Sander Mertens - Medium
				Medium
				https://ajmmertens.medium.com/why-vanilla-ecs-is-not-enough-d7ed4e3bebe5
				letzter zugriff: 25.04.2023 - 16:22

			d.	How to make a simple entity-component-system in C++
				(2020, 9. Februar)
				David Colson’s Blog
				https://www.david-colson.com/2020/02/09/making-a-simple-ecs.html
				letzter zugriff: 25.04.2023 - 16:28

	 IV:	Pathtracer

			a.	Peter Shirley
				(2020, Dezember)
				Ray Tracing in One Weekend
				https://raytracing.github.io/books/RayTracingInOneWeekend.html
				letzter zugriff: 25.04.2023 - 15:50

				Dieser großartige Blogeintrag liefert eine Übersicht über die Grundlagen des Raytracings, bzw. 
				genauer des Pathtracings. Sie war das Fundament meiner ersten Implementation und mein Einstieg
				ins Thema.

			b.	Sebastian Lague
				(2023, 1. April)
				Coding Adventure: Ray Tracing [Video]
				YouTube. https://www.youtube.com/watch?v=Qz0KTGYJtUk
				letzter zugriff: 25.04.2034 - 15:53

				In seinem Video experimentiert Lague mit Raytracing unteranderem auch auf der Grundlage von
				Shirleys Raytracing Blog.

			c.	Josh’s Channel
				(2022, 14. August)
				How Ray Tracing (Modern CGI) Works And How To Do It 600x Faster [Video]
				YouTube
				https://www.youtube.com/watch?v=gsZiJeaMO48
				letzter zugriff: 25.04.2023 - 15:57